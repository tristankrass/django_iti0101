# Generated by Django 2.1.2 on 2018-12-03 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20181203_1541'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='thumbnail',
            field=models.ImageField(default='mainBlog.png', upload_to='thumbnails/'),
        ),
    ]
